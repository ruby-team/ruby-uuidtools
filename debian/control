Source: ruby-uuidtools
Section: ruby
Priority: optional
Maintainer: Marc Dequènes (Duck) <Duck@DuckCorp.org>
Uploaders: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Build-Depends: debhelper-compat (= 13),
               gem2deb,
               libjs-jquery
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-uuidtools.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-uuidtools
XS-Ruby-Versions: all
Homepage: https://github.com/sporkmonger/uuidtools

Package: ruby-uuidtools
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby:any | ruby-interpreter,
         ${misc:Depends},
         ${shlibs:Depends}
Description: UUIDs generation library for Ruby
 UUIDTools was designed to be a simple library for generating any
 of the various types of UUIDs (or GUIDs if you prefer to call
 them that). It conforms to RFC 4122 whenever possible.

Package: ruby-uuidtools-doc
Section: doc
Architecture: all
Depends: libjs-jquery,
         ${misc:Depends}
Suggests: doc-base
Multi-Arch: foreign
Description: UUIDs generation library for Ruby - documentation
 UUIDTools was designed to be a simple library for generating any
 of the various types of UUIDs (or GUIDs if you prefer to call
 them that). It conforms to RFC 4122 whenever possible.
 .
 This is the Rdoc-generated documentation for the UUIDTools API.
